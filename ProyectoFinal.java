import java.util.Scanner;

class Main {
  public static void main(String[] args) 
{

    Scanner sc = new Scanner(System.in); 

    int CantTiquetes = Obtener_CantidadDeTiquetes(sc);
    int tiquetes[][] = generar_tiquetes(CantTiquetes);

    esperar_RealizarSorteo(sc); // espera hasta el momento del sorteo

    int granAcumulado = Obtener_GranAcumulado(sc);

    generarSorteo(tiquetes, CantTiquetes);

  }

  //leer o esperar parametros.
  public static int Obtener_CantidadDeTiquetes(Scanner sc)
  {
    //Lee la cantidad de tiquetes y lo regresa a su variable.
    int CantTiquetes = 0;

    while(CantTiquetes <= 0)
    {
      System.out.print("Ingrese la cantidad de tiquetes comprados: ");
      CantTiquetes = sc.nextInt();

      if(CantTiquetes <= 0)
      {
        System.out.println("Lo sentimos la cantidad no puede ser menor o igual que 0");
      }
    }

    return CantTiquetes;
  }

  public static int Obtener_GranAcumulado(Scanner sc)
  {
    //lee la cantidad del valor acumulado y lo regresa.
    int GranAcumulado = 0;

    while(GranAcumulado <= 4000)
    {
      System.out.print("Ingrese el valor del gran acumulado (debe ser mayor a 4000): ");
      GranAcumulado = sc.nextInt();

      if(GranAcumulado <= 4000)
      {
        System.out.println("Lo sentimos la cantidad debe ser mayor a 4000");
      }
    }

    return GranAcumulado;
  }

  public static void esperar_RealizarSorteo(Scanner sc)
  {
    //Espera a que el usuario de la opción de realizar el sorteo.

    char RealizarSorteo = ' ';

    while(RealizarSorteo != 'S' && RealizarSorteo != 's')
    {
      System.out.println("¿Desea realizar el sorteo ahora? (S/N)");
      RealizarSorteo = sc.next().charAt(0);

      if(RealizarSorteo != 'n' && RealizarSorteo != 'N' && RealizarSorteo != 'S' && RealizarSorteo != 's' )
      {
        System.out.println("Lo sentimos, la letra ingresada no corresponde con los caracteres esperados. para sí ingrese 'S' o 's'; para No ingrese 'n' o 'N'");
      }
     }

  }
  
  public static void generarSorteo(int tiquetes[][], int CantTiquetes)
  {
    int numero_ganador[] = generar_tiquete(); //arreglo unidimensional
    int contador_ganadores[] = new int[1] ; //la variable se contadora modifica en la funcion ObtenerGanadores.
    int ganadores[][] = ObtenerGanadores(numero_ganador, tiquetes, CantTiquetes, contador_ganadores);
    
    ordenar_Burbuja(ganadores, contador_ganadores[0]); //se ordenan los valores en un ordenamiento de burbuja.

    System.out.print("\nTiquete ganador:");
    imprimirTiquete(numero_ganador);
    
    imprimirGanadores(ganadores,tiquetes,contador_ganadores[0]);

  }

  public static int[][] ObtenerGanadores(int numero_ganador[],int tiquetes[][],int CantTiquetes,int Contador_ganadores[])
  {
    int cantAcierto, contador_ganadores = 0;
    boolean superBalota; 
    int ganadores[][] = new int [CantTiquetes][2];

    for(int i = 0; i < CantTiquetes; i++)
    {
      superBalota = false; 
      cantAcierto = 0;

      for(int q = 0; q < 6;q++){
        
          if(tiquetes[i][q] == numero_ganador[q])
          {
            if(q < 5)
            {
              cantAcierto += 1;
            }
            else{
              superBalota = true;
            }
          }
      }

      int premio = determinarPremio(cantAcierto, superBalota);

      if(premio != -1)
      {
        ganadores[contador_ganadores][0] = premio;
        ganadores[contador_ganadores][1] = i;
        contador_ganadores += 1;
      }
      
    }

    Contador_ganadores[0] = contador_ganadores; // C minuscula el contador local, con mayuscula el contador que se va a modificar en la funcion donde lo llaman.
    return ganadores;

  }

  public static int determinarPremio(int cantAcierto, boolean superBalota )
  {

    if (superBalota) 
    {
      switch (cantAcierto) 
      {
        case 5:
          return 1; // Gran acumulado 5+1
        case 4:
          return 2; // 4+1
        case 3:
          return 3; // 3+1
        case 2:
          return 4; // 2+1
        case 1:
          return 5; // 1+1
        case 0:
          return 6; // 0+1
      }
    }
    else 
    {
      switch (cantAcierto) 
      {
        case 5:
          return 7; // 5+0
        case 4:
          return 8; // 4+0
        case 3:
          return 9; // 3+0
      }
    }
      return -1;
  }

  public static int[][] generar_tiquetes(int cantidad)
  {
    int tiquetes[][] = new int[cantidad][6];

    for(int contador_tiquetes = 0; contador_tiquetes < cantidad ; contador_tiquetes ++ )
    {
      int tiquete[] = generar_tiquete();

      for(int x = 0 ; x < 6; x++)
      {
        tiquetes[contador_tiquetes][x] = tiquete[x];
      }
    }

    System.out.println("\n--! Los tiquetes han sido generados. !--\n");

    return tiquetes;
  }

  public static int[] generar_tiquete() 
  {
    int Numeros_generados[] = new int[6];

    int contador_generados = 0;
    int aleatorio;

    while (contador_generados < 6) 
    {
      if (contador_generados != 5) 
      {
        aleatorio = (int) Math.floor(Math.random() * 43 + 1);

        if (contador_generados == 0) 
        {
          Numeros_generados[0] = aleatorio;
          contador_generados += 1;
        }
        else
        {
          if (NumeroExistente(Numeros_generados, aleatorio, contador_generados) == false) 
          {
            Numeros_generados[contador_generados] = aleatorio;
            contador_generados += 1;
          }
        }
      } 
      else
      {
        aleatorio = (int) Math.floor(Math.random() * 16 + 1); //super balota
        Numeros_generados[5] = aleatorio;
        contador_generados += 1;
      }

    }
    return Numeros_generados;
  }

  public static boolean NumeroExistente(int generados[],int numero_aleatorio, int cant)
  {
    for(int x = 0; x < cant ; x++ )
    {
      if(generados[x] == numero_aleatorio)
      {
        return true;
      }
    }
    return false;
  }

  public static void ordenar_Burbuja(int ganadores[][], int contador_ganadores) 
  {
    int i, j, aux1, aux2;

    for (i = 0; i < contador_ganadores; i++) 
    {
      for (j = 0; j < contador_ganadores -i - 1; j++) 
      {
        if (ganadores[j + 1][0] < ganadores[j][0]) 
        {
          aux1 = ganadores[j + 1][0];
          aux2 = ganadores[j + 1][1];
          ganadores[j + 1][0] = ganadores[j][0];
          ganadores[j + 1][1] = ganadores[j][1];
          ganadores[j][0] = aux1;
          ganadores[j][1] = aux2;
        }
      }
    }
  }

  public static void imprimirGanadores(int ganadores[][], int tiquetes[][], int contador_ganadores )
  {
    int premios[] = {0,0,0,0,0,0,0,0,0}; //se marca con 1 los premios que tienen ganadores para indicar que ya fue visitado.
    int contador = 0;
    int seccionActual = 1;
    int numero_tiquete = 1;

    while(contador < contador_ganadores)
    {
      //De modo que la categoria empiece con otro valor, imprimirá que la seccion no tiene ganadores.
      if(seccionActual < ganadores[contador][0])
      {
        estadoSeccion(premios, ganadores[contador][0]);
        seccionActual = ganadores[contador][0];
      }
      //Cuando se haya mostrado en pantalla las categorias que no tienen ganadores. se imprimira la seccion. 
      //si no hubo ningun cambio en la ejecucion cuando inició no pasa nada
      if(premios[seccionActual-1] == 0)
      {
        seccionActual = ganadores[contador][0];
        System.out.println("\nGanadores " + obtener_seccionGanadores(seccionActual));
        premios[seccionActual-1] = 1; //lo marca como visitado
      }

      numero_tiquete = ganadores[contador][1];

      System.out.print("Tiquete # " + numero_tiquete + " :");
      imprimirTiquete(tiquetes[numero_tiquete]);

      contador += 1;
    }

    estadoSeccion(premios, 10); //en caso de no haber ganadores se imprime.
  
  }

  public static String obtener_seccionGanadores(int seccion)
  {
    switch (seccion) {
      case 1:
        return "GRAN ACUMULADO";
      case 2:
        return "4+1";
      case 3:
        return "3+1";
      case 4:
        return "2+1";
      case 5:
        return "1+1";
      case 6:
        return "0+1";
      case 7:
        return "5+0";
      case 8:
        return "4+0";
      case 9:
        return "3+0";
      default:
        return "Seccion no encontrada...";
    }
  }

  public static void estadoSeccion(int premios[],int tipoPremio)
  {
    for (int i = 0; i < tipoPremio-1; i++) 
    {
      if(premios[i] == 0)
      {
        System.out.println("\nGanadores " + obtener_seccionGanadores(i+1));
        System.out.println("-Seccion sin ganadores\n");
        premios[i] = 1;
      }
    }
  }

  public static void imprimirTiquete(int tiquete[])
  {
    for(int i = 0; i < 6 ; i++)
    {
      if(i == 0)
        System.out.print("["+tiquete[i] + ",");
      else if(i == 5)
        System.out.print(tiquete[i] + "]\n");
      else
        System.out.print(tiquete[i] + ",");
    }
  }
}
